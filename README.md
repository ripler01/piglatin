# Pig Latin

Translate a word to Pig Latin.


## Requirements
 - C compiler
 - Make
 - shelltest (apt install shelltestrunner)

## Build it
   `make build`

## Execute with:
   `./piglatin`

## Run tests
   `make test`

The output will look something like
```
$ make test
shelltest piglatin.test
:piglatin.test:1: [OK]
:piglatin.test:2: [OK]
:piglatin.test:3: [OK]
:piglatin.test:4: [OK]
:piglatin.test:5: [OK]
:piglatin.test:6: [OK]
:piglatin.test:7: [OK]
:piglatin.test:8: [OK]
:piglatin.test:9: [OK]

         Test Cases  Total      
 Passed  9           9          
 Failed  0           0          
 Total   9           9
```
