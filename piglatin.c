#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_WORD_SIZE 160

bool is_vowel(char character) {
   return (character == 'a' || character == 'e' || character == 'i' || character == 'o' || character == 'u' ||
       character == 'A' || character == 'E' || character == 'I' || character == 'O' || character == 'U' ? true : false);
}


void translate(char *word) {

   char translated[MAX_WORD_SIZE];

   if (is_vowel(word[0])) {
      strcat(word, "way");
      return;
   }
   
   int index_of_vowel = 0;
   for (int i=0; i < strlen(word); i++) {
      if (is_vowel(word[i])) {
         index_of_vowel = i;
         break;
      }
   }

   strncpy(translated, &word[index_of_vowel], strlen(word) + 1 - index_of_vowel);
   strncat(translated, word, index_of_vowel);
   strcat(translated, "ay");
   strcpy(word, translated);
}

int main(int argc, char *argv[]) {

   char word[MAX_WORD_SIZE];
   bool noWords = true;

   printf("Please enter a word: ");
   while (scanf("%s", word) > 0) {

      printf("You entered: %s\n", word);
      translate(word);
      printf("Translated word: %s\n", word);
      noWords = false;
   }

   if (noWords) {
      printf("No word entered.\n");
   }


   return 0;
}
