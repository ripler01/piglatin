
build:
	cc --version
	cc -o piglatin piglatin.c

test:
	shelltest --version
	shelltest --color piglatin.test

valgrind:
	echo "hello" | valgrind --leak-check=full --error-exitcode=1 ./piglatin

example:
	echo "cool beans" | ./piglatin | grep -Po 'Translated word: \K[A-z]+' | cowsay


check: test valgrind


clean:
	rm piglatin 


# vim: set noexpandtab
